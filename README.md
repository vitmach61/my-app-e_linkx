# Customer Management System

This helpful project is being created for you to have easier times with remembering all your customers name and email addresses.

## Installation

To have trouble-free experience with installation we`ve decided to use dockers.
Install the latest version of docker on your PC from [here](https://docs.docker.com/get-docker/) after that follow with installing docker compose [link here](https://docs.docker.com/compose/install/).

Continue with installing our project

run 

```$ sudo docker-compose up --build```

At this point you should be able to see the project in localhost on port 3010

## Working with CMS (Customer Management System)

Working with our system should be intuitive and simple.

To add a new customer simply click on a yellow button "add customer" and fill his name and email. Be careful with adding right email because you cannot change it later.

To delete a customer click on a yellow button with a cross.

You can also change customers name by double clicking it (the name). To save click on save or press Enter and to discard changes press Escape or click outside of customer box.



