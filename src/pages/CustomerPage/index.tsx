import React, {useState, useRef, KeyboardEvent} from 'react';
import useOutsideClick from "../../common/useOutsideClick";

export interface Customer {
    name: string,
    email: string
}

export interface CustomerProps {
    customer: Customer[]
    setCustomer: (c: Customer[]) => void;
}

interface CustomerBoxProps {
    customerData: Customer,
    setCustomerData: (newName: string, email: string) => void,
    deleteCustomer: (email: string) => void
}

interface SyntheticTarget extends EventTarget {
    value: string
}

interface SyntheticEvent {
    target: SyntheticTarget
}


export const CustomerPage = (props: CustomerProps) => {

    const [customerP, setCustomerP] = useState(props.customer)

    const customersList: JSX.Element[] = [];

    const changeCustomersName = (newName: string, email: string) => {
        const tempCustomers = customerP;
        tempCustomers.map(c => {
            if (c.email === email) {
                c.name = newName;
            }
            return c;
        });
        setCustomerP(tempCustomers);
    }

    const addCustomer = () => {
        const name = prompt("add name", "Dan");
        const email = prompt("add email", "who@email.com");

        if (name === null || email === null) {
            alert("name and email can`t be empty");
            return;
        }

        setCustomerP(customerP => [...customerP, {name: name, email: email}]);
    }

    const onDelete = (email: string) => {
        const tempCustomers = [...customerP];

        setCustomerP(
            tempCustomers.filter(c => {
                if (c.email !== email) {
                    return c;
                }
            })
        );
    }

    customerP.map((c) => {
        customersList.push(
            <CustomerBox
                key={c.email}
                customerData={c}
                setCustomerData={changeCustomersName}
                deleteCustomer={onDelete}
            />
        );
        return c;
    })

    return (
        <div className="customer__container">
            {
                customersList
            }
            <div className="customer__item">
                <button className="customer__item__addButton" onClick={addCustomer}>
                    add customer
                </button>
            </div>
        </div>
    )
}

const CustomerBox = (props: CustomerBoxProps) => {
    const [active, setActive] = useState(false);
    const [inputName, setInputName] = useState(props.customerData.name);

    const inputRef = useRef<HTMLInputElement>(null);

    React.useEffect(() => {
        if (active && inputRef.current) {
            inputRef.current.focus();
        }
    }, [active]);

    const discard = () => {
        setInputName(props.customerData.name);
        setActive(false);
    }

    useOutsideClick(inputRef, () => {
        discard();
    })

    const activate = () => {
        setActive(true)
    }

    const save = () => {
        props.setCustomerData(inputName, props.customerData.email);
        setActive(false);
    }

    const change = (event: SyntheticEvent) => {
        setInputName(event.target.value);
    }

    const onEnter = (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            save();
        } else if (event.key === "Escape") {
            discard();
        }
    }

    const onDelete = () => {
        props.deleteCustomer(props.customerData.email);
    }

    let name;

    if (active) {
        name =
            <div>
                <input
                    type="text"
                    value={inputName}
                    ref={inputRef}
                    onKeyDown={event => {
                        onEnter(event)
                    }}
                    onChange={event => {
                        change(event)
                    }}
                />
                <button className="customer__item__saveButton" onClick={save}>save</button>
            </div>
    } else {
        name =
            <div
                onDoubleClick={activate}
            >
                {props.customerData.name}
            </div>;
    }

    return (
        <div className="customer__item">
            {name}
            <div>{props.customerData.email}</div>
            <button className="customer__item__deleteButton" onClick={onDelete}>x</button>
        </div>
    )
}