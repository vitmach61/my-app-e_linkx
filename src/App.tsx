import React, {useState} from 'react';

import {CustomerPage, Customer} from './pages/CustomerPage'

import './styles/index.scss';

interface Data {
    customer: Customer[]
}

const data: Data = {
    customer: [
        {name: "Tamara", email: "tamarka@windowslive.com"},
        {name: "Vít", email: "vit.mach61@gmail.com"},
        {name: "Tom", email: "tomdan@gmail.com"},
        {name: "Radúz", email: "raduz@raduz.cz"},
        {name: "Radek", email: "radek5@seznam.cz"},
        {name: "Harvey", email: "suits@outlook.com"},
        {name: "Daniela", email: "motyl@seznam.cz"}
    ]
}

function App() {
    const [customers, setCustomers] = useState(data.customer);

    const sc = (c: Customer[]) => {
        setCustomers(c);
    }


    return (
        <div className="wrapper">
            <h2>
                Customers
            </h2>
            <CustomerPage
                customer={customers}
                setCustomer={c => setCustomers(c)}
            />
        </div>
    );
}

export default App;
