# pull official base image
FROM node:alpine

# set working directory
WORKDIR /app

# install app dependencies
COPY package.json /app
#COPY package-lock.json ./

RUN npm install --silent
#RUN npm install react-scripts@3.4.1 -g --silent

# add app
COPY . /app

# start app
CMD ["npm", "start"]